﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class WebCam_3cluster : MonoBehaviour {
    public Camera maincamera;
    public Canvas canvas;
    WebCamTexture webcamTexture;
	const int FPS = 30;

    Texture2D texture;
    Color32[] colors = null;

    //ランダムな区切りを設定
    
    System.Random rnd = new System.Random();

    IEnumerator Init()
    {
        while (true)
        {
            if (webcamTexture.width > 16 && webcamTexture.height > 16)
            {
                colors = new Color32[webcamTexture.width * webcamTexture.height];
                texture = new Texture2D(webcamTexture.width, webcamTexture.height, TextureFormat.RGBA32, false);
                GetComponent<Image>().material.mainTexture = texture;
                break;
            }
            yield return null;
        }
    }

    void Start(){

        //カメラのテクスチャをQuadに載せる
        if (WebCamTexture.devices.Length > 0){
			WebCamDevice cam = WebCamTexture.devices[0];
			WebCamTexture wcam = new WebCamTexture (cam.name);
			wcam.Play ();
			int width = wcam.width/2, height = wcam.height/2;

            //カメラ画像の比率に応じて変形
            float _w = canvas.GetComponent<RectTransform>().rect.width*2/3;
            float _h = _w /(float)width*(float)height;
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(_w, _h);

            //if(width<1280||height<720){width*=2;height*=2;}
            webcamTexture = new WebCamTexture (cam.name, width, height, FPS);
			wcam.Stop ();

            GetComponent<Image>().material.mainTexture = webcamTexture;
			webcamTexture.Play();
		}

        StartCoroutine(Init());
    }
    
    void Update()
    {
        if (colors != null)
        {
            webcamTexture.GetPixels32(colors);

            int width = webcamTexture.width;
            int height = webcamTexture.height;

            Color32[] output_colors = new Color32[webcamTexture.width * webcamTexture.height];
            Color32[] cluster_colors = new Color32[webcamTexture.width * webcamTexture.height];


            int roughness = 1;

            for (int x = roughness; x < width - roughness; x += roughness)
            {
                for (int y = roughness; y < height - roughness; y += roughness)
                {
                    double sum_r = 0, sum_b = 0, sum_g = 0;
                    int count = 0;
                    for (int i = 0; i < roughness; i++)
                    {
                        for (int j = 0; j < roughness; j++)
                        {
                            count++;
                            sum_r += colors[(x + i) + (y + j) * width].r;
                            sum_g += colors[(x + i) + (y + j) * width].g;
                            sum_b += colors[(x + i) + (y + j) * width].b;

                        }
                    }

                    sum_r = sum_r / count;
                    sum_g = sum_g / count;
                    sum_b = sum_b / count;
                    



                    Color32 c = new Color();
                    c.r = (byte)((sum_r + cluster_colors[x + y * width].r) /2);
                    c.g = (byte)((sum_g + cluster_colors[x + y * width].g) / 2);
                    c.b = (byte)((sum_b + cluster_colors[x + y * width].b) / 2);
                    c.a=255;

                    c = toGray(c);

                    for (int i = 0; i < roughness; i++)
                    {
                        for (int j = 0; j < roughness; j++)
                        {
                            output_colors[(x + i) + (y + j) * width] = c;
                        }
                    }
                }
            }

            output_colors.CopyTo(colors,0);
            //エッジ抽出
            for (int x = roughness; x < width - roughness; x += roughness)
            {
                for (int y = roughness; y < height - roughness; y += roughness)
                {
                    int sum=0;
                    sum += colors[(x - roughness) + (y - roughness) * width].r * -1;
                    sum += colors[(x - roughness) + (y + 0) * width].r * -2;
                    sum += colors[(x - roughness) + (y + roughness) * width].r * -1;

                    sum += colors[(x + roughness) + (y - roughness) * width].r * 1;
                    sum += colors[(x + roughness) + (y + 0) * width].r * 2;
                    sum += colors[(x + roughness) + (y + roughness) * width].r * 1;

                    sum += colors[(x - roughness) + (y - roughness) * width].r * -1;
                    sum += colors[(x + 0) + (y - roughness) * width].r * -2;
                    sum += colors[(x + roughness) + (y - roughness) * width].r * -1;

                    sum += colors[(x - roughness) + (y + roughness) * width].r * 1;
                    sum += colors[(x + 0) + (y + roughness) * width].r * 2;
                    sum += colors[(x + roughness) + (y + roughness) * width].r * 1;

                    Color32 c = new Color();
                    c.r = c.b = c.g = (byte)Math.Min(Math.Max(Math.Abs(sum),0),255);
                    c.a = 255;

                    for (int i = 0; i < roughness; i++)
                    {
                        for (int j = 0; j < roughness; j++)
                        {
                            output_colors[(x + i) + (y + j) * width] = c;
                        }
                    }
                }
            }
            //クラスタリング
            int[] cluster_numbers = new int[height*width];
            int cluster_number = 1;
            Color32 cluster_color = getRandomColor(1000+cluster_number);
            //ArrayList clusters = new ArrayList();
            for (int y = roughness; y < height - roughness; y += roughness)
            {
                //ColorCluster tmp = new ColorCluster(cluster_number);
                for (int x = roughness; x < width - roughness; x += roughness)
                {
                    Color32 c = output_colors[x + y * width];
                    //もしエッジなら要素に入れず、新しいクラスタを作る
                    if (c.r > 10)
                    {
                        cluster_number++;
                        cluster_color = getRandomColor(1000 + cluster_number);
                    }
                    //エッジでないなら要素に入れる
                    else
                    {
                        cluster_colors[x + y * width] = cluster_color;
                        cluster_numbers[x + y * width] = cluster_number;
                    }

                    c.r = (byte)((c.r + cluster_colors[x + y * width].r) / 2);
                    c.g = (byte)((c.g + cluster_colors[x + y * width].g) / 2);
                    c.b = (byte)((c.b + cluster_colors[x + y * width].b) / 2);

                    for (int i = 0; i < roughness; i++)
                    {
                        for (int j = 0; j < roughness; j++)
                        {
                            output_colors[(x + i) + (y + j) * width] = c;
                        }
                    }
                }
            }
            for (int y = roughness; y < height - roughness; y += roughness)
            {
                for (int x = roughness; x < width - roughness; x += roughness)
                {
                    //今見ているクラスタと、次にあるところのクラスタを併合する
                    //今見ているクラスタの番号
                    int tmp_number = cluster_numbers[x + y * width];
                    //直下にあるドットがエッジでないなら併合開始
                    if ( cluster_numbers[x + (y + roughness) * width] != 0 && tmp_number != 0)
                    {
                        cluster_numbers[x + (y + roughness) * width] = tmp_number;
                        cluster_colors[x + (y + roughness) * width] = cluster_colors[x + y * width];
                        //ここから左右に走査して、同じように併合する
                        
                        //まずは左側
                        for (int dx = x; dx > roughness; dx -= roughness)
                        {
                            if (cluster_numbers[dx - roughness + (y + roughness) * width] == 0)
                            {
                                break;
                            }
                            if (cluster_numbers[dx - roughness + (y + roughness) * width] != tmp_number)
                            {
                                cluster_numbers[dx - roughness + (y + roughness) * width] = tmp_number;
                                cluster_colors[dx - roughness + (y + roughness) * width] = cluster_colors[x + y * width];
                            }
                        }
                        //右側
                        for (int dx = x; dx < width - roughness; dx += roughness)
                        {
                            if (cluster_numbers[dx + roughness + (y + roughness) * width] == 0)
                            {
                                break;
                            }
                            if (cluster_numbers[dx + roughness + (y + roughness) * width] != tmp_number)
                            {
                                cluster_numbers[dx + roughness + (y + roughness) * width] = tmp_number;
                                cluster_colors[dx + roughness + (y + roughness) * width] = cluster_colors[x + y * width];
                            }
                        }
                    }
                    Color32 c = output_colors[x + y * width];
                    c.r = cluster_colors[x + y * width].r;
                    c.g = cluster_colors[x + y * width].g;
                    c.b = cluster_colors[x + y * width].b;

                    for (int i = 0; i < roughness; i++)
                    {
                        for (int j = 0; j < roughness; j++)
                        {
                            output_colors[(x + i) + (y + j) * width] = c;
                        }
                    }
                }
            }

                //drawRect(output_colors, width, width / 2-50, height / 2-50, 100, 100);
                texture.SetPixels32(output_colors);
            texture.Apply();
            
        }
    }
    void drawRect(Color32[] colors,int width, int x, int y, int x_size, int y_size)
    {
        for (int i = 0; i < x_size; i++)
        {
            for (int j = 0; j < y_size; j++)
            {
                Color32 c = new Color32();
                c.r = colors[(x + i) + (y + j) * width].r;
                c.g = 0;
                c.b = 0;
                c.a = 255;
                colors[(x + i) + (y + j) * width] = c;
            }
        }
    }

    Color32 toGray(Color32 c)
    {
        byte gray = (byte)(0.1f * c.r + 0.7f * c.g + 0.2f * c.b);
        c.r = c.g = c.b = gray;
        c.a = 255;
        return c;
    }
    double[] HSVtoXY(double[] HSV)
    {
        double X = Math.Cos(HSV[0] * Math.PI / 180) * HSV[1];
        double Y = Math.Sin(HSV[0] * Math.PI / 180) * HSV[1];

        return new double[]{ X,Y};
    }

    double[] toHSV(Color32 c)
    {
        double H, S, V;
        double max = Math.Max(c.r, Math.Max(c.b, c.g));
        double min = Math.Min(c.r, Math.Min(c.b, c.g));
        if (c.r > c.b && c.r > c.g)
        {
            H = 60 * ((c.g - c.b) / (max - min));
        }
        else if (c.g > c.b)
        {
            H = 60 * ((c.b - c.r) / (max - min));
        }
        else if (c.b > c.g)
        {
            H = 60 * ((c.r - c.g) / (max - min)); ;
        }
        else
        {
            H = 0;
        }
        if (H < 0) H += 360;

        S = (max - min) / max;
        V = max;

        return new double[] { H, S, V };
    }

    Color32 getRandomColor(int seed)
    {
        Color32 c = new Color32();
        System.Random rnd = new System.Random(seed);
        c.r = (byte)(rnd.NextDouble() * 255);
        c.g = (byte)(rnd.NextDouble() * 255);
        c.b = (byte)(rnd.NextDouble() * 255);
        c.a = 255;
        return c;
    }
}

public class ColorCluster{
    public ArrayList elements;
    int cluster_number;
    public Color32 c;
    public ColorCluster(int cluster_number)
    {
        elements = new ArrayList();
        this.cluster_number = cluster_number;
        c = getRandomColor(cluster_number+1000);
    }
    Color32 getRandomColor(int seed)
    {
        Color32 c = new Color32();
        System.Random rnd = new System.Random(seed);
        c.r = (byte)(rnd.NextDouble() * 255);
        c.g = (byte)(rnd.NextDouble() * 255);
        c.b = (byte)(rnd.NextDouble() * 255);
        c.a = 255;
        return c;
    }
}

public class ColorClusterElement
{
    public int x, y, cluster_number;
    Color32 c;
    public ColorClusterElement(int x, int y, int cluster_number)
    {
        this.x = x;
        this.y = y;
        this.cluster_number = cluster_number;
    }
}