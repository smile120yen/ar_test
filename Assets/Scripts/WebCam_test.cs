﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class WebCam_test : MonoBehaviour {
    public Camera maincamera;
    public Canvas canvas;
    WebCamTexture webcamTexture;
	const int FPS = 30;

    Texture2D texture;
    Color32[] colors = null;

    //ハフ変換用
    const int THETA_MAX = 64;
    const int RHO_MAX = 400;
    float PIK = (float)Math.PI / THETA_MAX;
    //三角関数テーブル（サイン）
    float[] sn = new float[THETA_MAX];
    //三角関数テーブル（コサイン）
    float[] cs = new float[THETA_MAX];
    //変数
    int rho, theta;
    short[][] counter = new short[THETA_MAX][];

    IEnumerator Init()
    {
        while (true)
        {
            if (webcamTexture.width > 16 && webcamTexture.height > 16)
            {
                colors = new Color32[webcamTexture.width * webcamTexture.height];
                texture = new Texture2D(webcamTexture.width, webcamTexture.height, TextureFormat.RGBA32, false);
                GetComponent<Image>().material.mainTexture = texture;
                break;
            }
            yield return null;
        }
    }

    void Start(){

        //ハフ変換用配列
        //三角関数テーブルを作成
        for (int i = 0; i < THETA_MAX; i++)
        {
            sn[i] = (float)Math.Sin(PIK * i);
            cs[i] = (float)Math.Cos(PIK * i);
        }
        //ハフ変換用配列を定義
        for (int i = 0; i < THETA_MAX; i++)
        {
            counter[i] = new short[2 * RHO_MAX];
        }


        //Quadを画面いっぱいに広げる
        //float _h = maincamera.orthographicSize * 2;
        //float _h = canvas.GetComponent<RectTransform>().rect.height;
        //float _w = _h * maincamera.aspect;
        //float _w = canvas.GetComponent<RectTransform>().rect.width/3;
        //float _h = _w / maincamera.aspect;



        //スマホ(Unity)が横ならそのまま
        //transform.localScale = new Vector3(_w, _h, 1);
        //transform.localRotation *= Quaternion.Euler(0, 0, 180);
        //this.GetComponent<RectTransform>().sizeDelta = new Vector2(_w, _h);


        //カメラのテクスチャをQuadに載せる
        if (WebCamTexture.devices.Length > 0){
			WebCamDevice cam = WebCamTexture.devices[0];
			WebCamTexture wcam = new WebCamTexture (cam.name);
			wcam.Play ();
			int width = wcam.width/2, height = wcam.height/2;

            //カメラ画像の比率に応じて変形
            float _w = canvas.GetComponent<RectTransform>().rect.width/3;
            float _h = _w /(float)width*(float)height;
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(_w, _h);

            //if(width<1280||height<720){width*=2;height*=2;}
            webcamTexture = new WebCamTexture (cam.name, width, height, FPS);
			wcam.Stop ();

            GetComponent<Image>().material.mainTexture = webcamTexture;
			webcamTexture.Play();
		}

        StartCoroutine(Init());
    }
    
    void Update()
    {
        if (colors != null)
        {
            webcamTexture.GetPixels32(colors);

            int width = webcamTexture.width;
            int height = webcamTexture.height;
            Debug.Log(width + "," + height);
            Color32[] output_colors = new Color32[webcamTexture.width * webcamTexture.height]; ;

            
            for (int x = 1; x < width - 1; x++)
            {
                for (int y = 1; y < height - 1; y++)
                {
                    float sum = 0;
                    int count = 0;
                    Color32 c = new Color32();
                    for (int xi = -1; xi <= 1; xi++)
                    {
                        for (int yi = -1; yi <= 1; yi++)
                        {
                            count++;
                            sum += toGray(colors[(x + xi) + (y + yi) * width]).r;
                        }
                    }
                    sum /= count;
                    c.r = c.b = c.g = (byte)sum;
                    output_colors[x + y * width] =c;
                }
            }
            
            //output_colors.CopyTo(colors, 0);
            /*
            for (int x = 1; x < width-1; x++)
            {
                for (int y = 1; y < height-1; y++)
                {
                    Color32 c = new Color32();
                    float sum_r = 0;
                    float sum_b = 0;
                    float sum_g = 0;
                    int count=0;
                    for (int xi = -1; xi <= 1; xi++)
                    {
                        for (int yi = -1; yi <= 1; yi++)
                        {
                            count++;
                            sum_r += (colors[(x + xi) + (y + yi) * width].r);
                            sum_g+= (colors[(x + xi) + (y + yi) * width].g);
                            sum_b += (colors[(x + xi) + (y + yi) * width].b);
                        }
                    }
                    sum_r /= count;
                    sum_g /= count;
                    sum_b /= count;

                    c.r = (byte)sum_r;
                    c.g = (byte)sum_g;
                    c.b = (byte)sum_b;
                    c.a = 255;

                    output_colors[x + y * width] = c;
                }
            }
            output_colors.CopyTo(colors, 0);
            */
            /*
            for (int x = 1; x < width - 1; x++)
            {
                for (int y = 1; y < height - 1; y++)
                {
                    Color c = new Color();
                    
                    float dx = 0;
                    dx += (colors[(x - 1) + (y - 1) * width].r * -1);
                    dx += (colors[(x - 1) + (y + 0) * width].r * -1);
                    dx += (colors[(x - 1) + (y + 1) * width].r * -1);
                    dx += (colors[(x + 1) + (y - 1) * width].r * 1);
                    dx += (colors[(x + 1) + (y + 0) * width].r * 1);
                    dx += (colors[(x + 1) + (y + 1) * width].r * 1);

                    float dy = 0;
                    dy += (colors[(x - 1) + (y - 1) * width].r * -1);
                    dy += (colors[(x + 0) + (y - 1) * width].r * -1);
                    dy += (colors[(x + 1) + (y - 1) * width].r * -1);
                    dy += (colors[(x - 1) + (y + 1) * width].r * 1);
                    dy += (colors[(x + 0) + (y + 1) * width].r * 1);
                    dy += (colors[(x + 1) + (y + 1) * width].r * 1);

                    c.r = (byte)(Math.Sqrt(dx*dx+dy*dy)/20);
                    c.b = c.g = c.r;
                    
                    //ハフ変換
                    //全体を走査して、白い部分をハフ変換しつつ格納
                    
                    
                    if (c.r > 0.5)
                    {
                        for (theta = 0; theta < THETA_MAX; theta++)
                        {
                            rho = (int)(x * cs[theta] + y * sn[theta] + 0.5);
                            counter[theta][rho + RHO_MAX]++;
                        }
                    }

                    

                    
                    
                    dx = 0;
                    dx += (colors[(x - 1) + (y - 1) * width].g * -1);
                    dx += (colors[(x - 1) + (y + 0) * width].g * -1);
                    dx += (colors[(x - 1) + (y + 1) * width].g * -1);
                    dx += (colors[(x + 1) + (y - 1) * width].g * 1);
                    dx += (colors[(x + 1) + (y + 0) * width].g * 1);
                    dx += (colors[(x + 1) + (y + 1) * width].g * 1);

                    dy = 0;
                    dy += (colors[(x - 1) + (y - 1) * width].g * -1);
                    dy += (colors[(x + 0) + (y - 1) * width].g * -1);
                    dy += (colors[(x + 1) + (y - 1) * width].g * -1);
                    dy += (colors[(x - 1) + (y + 1) * width].g * 1);
                    dy += (colors[(x + 0) + (y + 1) * width].g * 1);
                    dy += (colors[(x + 1) + (y + 1) * width].g * 1);

                    c.g = (byte)(Math.Sqrt(dx * dx + dy * dy) / 30);

                    dx = 0;
                    dx += (colors[(x - 1) + (y - 1) * width].b * -1);
                    dx += (colors[(x - 1) + (y + 0) * width].b * -1);
                    dx += (colors[(x - 1) + (y + 1) * width].b * -1);
                    dx += (colors[(x + 1) + (y - 1) * width].b * 1);
                    dx += (colors[(x + 1) + (y + 0) * width].b * 1);
                    dx += (colors[(x + 1) + (y + 1) * width].b * 1);

                    dy = 0;
                    dy += (colors[(x - 1) + (y - 1) * width].b * -1);
                    dy += (colors[(x + 0) + (y - 1) * width].b * -1);
                    dy += (colors[(x + 1) + (y - 1) * width].b * -1);
                    dy += (colors[(x - 1) + (y + 1) * width].b * 1);
                    dy += (colors[(x + 0) + (y + 1) * width].b * 1);
                    dy += (colors[(x + 1) + (y + 1) * width].b * 1);

                    c.b = (byte)(Math.Sqrt(dx * dx + dy * dy) / 30);
                    
                    c.a = 255;

                    output_colors[x + y * width] = c;
                }
            }

            output_colors.CopyTo(colors, 0);
            */

            //逆ハフ変換
            /*
            int counter_max;
            int theta_max = 0;
            int rho_max = -RHO_MAX;

            int end_flag = 0;
            int line_count = 0;

            int XMAX = width;
            int YMAX = height;

            do
            {
                line_count++;
                counter_max = 0;
                //counterが最大になるtheta_maxとrho_maxを求める
                for (theta = 0; theta < THETA_MAX; theta++)
                    for (rho = -RHO_MAX; rho < RHO_MAX; rho++)
                        if (counter[theta][rho + RHO_MAX] > counter_max)
                        {
                            counter_max = counter[theta][rho + RHO_MAX];
                            //60ピクセル以下の直線になれば検出を終了
                            if (counter_max <= 100) end_flag = 1;
                            else end_flag = 0;
                            theta_max = theta;
                            rho_max = rho;
                        }

                //検出した直線の描画
                //xを変化させてyを描く（垂直の線を除く）
                if (theta_max != 0)
                {
                    for (int x = 0; x < XMAX; x++)
                    {
                        int y = (int)((rho_max - x * cs[theta_max]) / sn[theta_max]);
                        if (y >= YMAX || y < 0) continue;
                        //直線描画(X1+x,Y0+y,0,0)
                        drawPoint(output_colors, width, x,y);
                    }
                }

                //yを変化させてxを描く（水平の線を除く）
                if (theta_max != THETA_MAX / 2)
                {
                    for (int y = 0; y < YMAX; y++)
                    {
                        int x = (int)((rho_max - y * sn[theta_max]) / cs[theta_max]);
                        if (x >= XMAX || x < 0) continue;
                        //直線描画(X1 + x, Y0 + y, 0, 0);
                        drawPoint(output_colors, width, x,y);
                    }
                }

                //近傍の直線を消す
                
                for (int j = -10; j <= 10; j++)
                    for (int i = -30; i <= 30; i++)
                    {
                        if (theta_max + i < 0)
                        {
                            theta_max += THETA_MAX;
                            rho_max = -rho_max;
                        }
                        if (theta_max + i >= THETA_MAX)
                        {
                            theta_max -= THETA_MAX;
                            rho_max = -rho_max;
                        }
                        if (rho_max + j < -RHO_MAX || rho_max + j >= RHO_MAX)
                            continue;
                        counter[theta_max + i][rho_max + RHO_MAX + j] = 0;
                    }
                //長さが60ピクセル以下か、直線が10本検出されたら終了
            } while (end_flag == 0 && line_count < 200);
            */
            //output_colors.CopyTo(colors, 0); 

            texture.SetPixels32(output_colors);
            texture.Apply();
        }
    }

    void drawLine(Color32[] colors,int width,int x1,int y1,int x2,int y2)
    {
        for( int x=x1; x<x2; x++)
        {
            for (int y = y1; y < y2; y++)
            {
                colors[x + y * width].r = 255;
                colors[x + y * width].g = 0;
                colors[x + y * width].b = 0;
            }
        }
    }
    void drawPoint(Color32[] colors, int width, int x, int y)
    {
       colors[x + y * width].r = 255;
       colors[x + y * width].g = 0;
       colors[x + y * width].b = 0;
    }


    Color32 toGray(Color32 c)
    {
        byte gray = (byte)(0.1f * c.r + 0.7f * c.g + 0.2f * c.b);
        c.r = c.g = c.b = gray;
        c.a = 255;
        return c;
    }

    Color32 likeGB(Color32 c,int split)
    {
        byte gray = (byte)(0.1f * c.r + 0.7f * c.g + 0.2f * c.b);
        c.r = c.g = c.b = gray;

        for (int i = 0; i < split; i++)
        {
            float col1 = i * (255 / (float)split);
            float col2 = (i + 1f) * (255 / (float)split);



            if (col1 <= c.r && c.r <= col2)
            {
                c.r = (byte)((col1 + col2) / 2f);
            }
        }
        c.a = 255;
        return c;
    }
}