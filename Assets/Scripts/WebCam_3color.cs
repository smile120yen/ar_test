﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.UI;

public class WebCam_3color : MonoBehaviour {
    public Camera maincamera;
    public Canvas canvas;
    WebCamTexture webcamTexture;
	const int FPS = 30;

    Texture2D texture;
    Color32[] colors = null;
    Color32[] cluster_colors = null;
    //ランダムな区切りを設定
    //区切りを移動させることによって尤度が高くなるなら移動、みたいな
    Color32 bottom = new Color32();
    Color32 top = new Color32();
    //重心を設定？
    //
    Color32 c1 = new Color32();
    Color32 c2 = new Color32();
    Color32 c3 = new Color32();
    Color32 n_c1 = new Color32();
    Color32 n_c2 = new Color32();
    Color32 n_c3 = new Color32();
    System.Random rnd = new System.Random();

    IEnumerator Init()
    {
        while (true)
        {
            if (webcamTexture.width > 16 && webcamTexture.height > 16)
            {
                colors = new Color32[webcamTexture.width * webcamTexture.height];
                cluster_colors = new Color32[webcamTexture.width * webcamTexture.height];

                top.r = (byte)(rnd.NextDouble() * 256);
                top.g = (byte)(rnd.NextDouble() * 256);
                top.b = (byte)(rnd.NextDouble() * 256);

                bottom.r = (byte)(rnd.NextDouble() * top.r);
                bottom.g = (byte)(rnd.NextDouble() * top.g);
                bottom.b = (byte)(rnd.NextDouble() * top.b);

                texture = new Texture2D(webcamTexture.width, webcamTexture.height, TextureFormat.RGBA32, false);
                GetComponent<Image>().material.mainTexture = texture;
                break;
            }
            yield return null;
        }
    }

    void Start(){

        //カメラのテクスチャをQuadに載せる
        if (WebCamTexture.devices.Length > 0){
			WebCamDevice cam = WebCamTexture.devices[0];
			WebCamTexture wcam = new WebCamTexture (cam.name);
			wcam.Play ();
			int width = wcam.width/2, height = wcam.height/2;

            //カメラ画像の比率に応じて変形
            float _w = canvas.GetComponent<RectTransform>().rect.width*2/3;
            float _h = _w /(float)width*(float)height;
            this.GetComponent<RectTransform>().sizeDelta = new Vector2(_w, _h);

            //if(width<1280||height<720){width*=2;height*=2;}
            webcamTexture = new WebCamTexture (cam.name, width, height, FPS);
			wcam.Stop ();

            GetComponent<Image>().material.mainTexture = webcamTexture;
			webcamTexture.Play();
		}

        StartCoroutine(Init());
    }
    
    void Update()
    {
        if (colors != null)
        {
            webcamTexture.GetPixels32(colors);

            int width = webcamTexture.width;
            int height = webcamTexture.height;

            Color32[] output_colors = new Color32[webcamTexture.width * webcamTexture.height]; ;



            int roughness = 2;
            //int average_count=0;
            //Color average_color = new Color();

            //なんかいい感じにTopとBottomを更新する
            //Top広げてみて増えるかチェック？
            int c1_count=0;
            int c2_count = 0;
            int c3_count = 0;

            for (int x = roughness; x < width - roughness; x += roughness)
            {
                for (int y = roughness; y < height - roughness; y += roughness)
                {
                    double sum_r = 0, sum_b = 0, sum_g = 0;
                    int count = 0;
                    for (int i = -roughness; i < roughness; i++)
                    {
                        for (int j = -roughness; j < roughness; j++)
                        {
                            count++;
                            sum_r += colors[(x + i) + (y + j) * width].r;
                            sum_g += colors[(x + i) + (y + j) * width].g;
                            sum_b += colors[(x + i) + (y + j) * width].b;

                            /*
                            if (average_count == 0) average_color = colors[(x + i) + (y + j) * width];
                            average_count++;
                            average_color.r = (average_color.r*(average_count-1) + (colors[(x + i) + (y + j) * width]).r)/average_count;
                            average_color.g = (average_color.g * (average_count - 1) + (colors[(x + i) + (y + j) * width]).g) / average_count;
                            average_color.b = (average_color.b * (average_count - 1) + (colors[(x + i) + (y + j) * width]).b) / average_count;
                            */
                        }
                    }

                    sum_r = sum_r / count;
                    sum_g = sum_g / count;
                    sum_b = sum_b / count;

                    if (top.r < sum_r && top.g < sum_g && top.b < sum_b)
                    {
                        if (c1_count == 0) c1 = n_c1;
                        n_c1.r = (byte)((c1.r * c1_count + colors[x + y * width].r)/ (c1_count+1));
                        n_c1.g = (byte)((c1.g * c1_count + colors[x + y * width].g) / (c1_count+1));
                        n_c1.b = (byte)((c1.b * c1_count + colors[x + y * width].b) / (c1_count+1));
                        c1.r = 255;
                        c1_count++;
                        cluster_colors[x + y * width] = c1;
                    }else if (bottom.r > sum_r && bottom.g > sum_g && bottom.b >sum_b)
                    {
                        if (c3_count == 0) c3 = n_c3;
                        n_c3.r = (byte)((c3.r * c3_count + colors[x + y * width].r) / (c3_count+1));
                        n_c3.g = (byte)((c3.g * c3_count + colors[x + y * width].g) / (c3_count+1));
                        n_c3.b = (byte)((c3.b* c3_count + colors[x + y * width].b) / (c3_count+1));
                        c3.b = 255;
                        c3_count++;
                        cluster_colors[x + y * width] = c3;
                    }
                    else
                    {
                        if (c2_count == 0) c2 = n_c2;
                        n_c2.r = (byte)((c2.r * c2_count + colors[x + y * width].r)/(c2_count+1));
                        n_c2.g = (byte)((c2.g * c2_count + colors[x + y * width].g) / (c2_count+1));
                        n_c2.b = (byte)((c2.b * c2_count + colors[x + y * width].b) / (c2_count+1));
                        c2_count++;
                        cluster_colors[x + y * width] = c2;
                    }



                    Color32 c = new Color();
                    c.r = (byte)((sum_r + cluster_colors[x + y * width].r) /2);
                    c.g = (byte)((sum_g + cluster_colors[x + y * width].g) / 2);
                    c.b = (byte)((sum_b + cluster_colors[x + y * width].b) / 2);
                    c.a=255;

                    for (int i = -roughness; i < roughness; i++)
                    {
                        for (int j = -roughness; j < roughness; j++)
                        {
                            output_colors[(x + i) + (y + j) * width] = c;

                            //cluster_colors
                        }
                    }
                }
            }

            //今は「同じ大きさ」を基準としている
            //分散を基準にすればもっといい？
            if (c2_count < c1_count)
            {
                top.r = (byte)Math.Min((int)top.r + rnd.NextDouble() * 5, 255);
                top.g = (byte)Math.Min((int)top.g + rnd.NextDouble() * 5, 255);
                top.b = (byte)Math.Min((int)top.b + rnd.NextDouble() * 5, 255);
            }

            if (c2_count < c3_count) {
                bottom.r = (byte)Math.Max((int)bottom.r - rnd.NextDouble() * 5, 0);
                bottom.g = (byte)Math.Max((int)bottom.g - rnd.NextDouble() * 5, 0);
                bottom.b = (byte)Math.Max((int)bottom.b - rnd.NextDouble() * 5, 0);
            }

            if (c2_count > c1_count)
            {
                top.r = (byte)Math.Max((int)top.r - rnd.NextDouble() * 5, 0);
                top.g = (byte)Math.Max((int)top.g - rnd.NextDouble() * 5, 0);
                top.b = (byte)Math.Max((int)top.b - rnd.NextDouble() * 5, 0);
            }

            if (c2_count > c3_count)
            {
                bottom.r = (byte)Math.Min((int)bottom.r + rnd.NextDouble() * 5, 255);
                bottom.g = (byte)Math.Min((int)bottom.g + rnd.NextDouble() * 5, 255);
                bottom.b = (byte)Math.Min((int)bottom.b + rnd.NextDouble() * 5, 255);
            }

            Debug.Log("B" + bottom.r + "," + bottom.g + "," + bottom.b);
            Debug.Log("T" + top.r + "," + top.g + "," + top.b);
            
            //drawRect(output_colors, width, width / 2-50, height / 2-50, 100, 100);
            texture.SetPixels32(output_colors);
            texture.Apply();
            
        }
    }
    void drawRect(Color32[] colors,int width, int x, int y, int x_size, int y_size)
    {
        for (int i = 0; i < x_size; i++)
        {
            for (int j = 0; j < y_size; j++)
            {
                Color32 c = new Color32();
                c.r = colors[(x + i) + (y + j) * width].r;
                c.g = 0;
                c.b = 0;
                c.a = 255;
                colors[(x + i) + (y + j) * width] = c;
            }
        }
    }

    Color32 toGray(Color32 c)
    {
        byte gray = (byte)(0.1f * c.r + 0.7f * c.g + 0.2f * c.b);
        c.r = c.g = c.b = gray;
        c.a = 255;
        return c;
    }
    double[] HSVtoXY(double[] HSV)
    {
        double X = Math.Cos(HSV[0] * Math.PI / 180) * HSV[1];
        double Y = Math.Sin(HSV[0] * Math.PI / 180) * HSV[1];

        return new double[]{ X,Y};
    }

    double[] toHSV(Color32 c)
    {
        double H, S, V;
        double max = Math.Max(c.r, Math.Max(c.b, c.g));
        double min = Math.Min(c.r, Math.Min(c.b, c.g));
        if (c.r > c.b && c.r > c.g)
        {
            H = 60 * ((c.g - c.b) / (max - min));
        }
        else if (c.g > c.b)
        {
            H = 60 * ((c.b - c.r) / (max - min));
        }
        else if (c.b > c.g)
        {
            H = 60 * ((c.r - c.g) / (max - min)); ;
        }
        else
        {
            H = 0;
        }
        if (H < 0) H += 360;

        S = (max - min) / max;
        V = max;

        return new double[] { H, S, V };
    }
}